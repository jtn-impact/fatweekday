# Fatweekday

# NodeJS
For at køre projektet skal du bruge NodeJS. Hent den nyeste version fra: https://nodejs.org/en/ og installér den.

# IDE/Editor
Hent din fortrukne editor. Foreslår Microsoft Visual Studio Code

#Extensions Visual Studio Code (optional)
- Auto Import
- Beautify
- C#
- GraphQL for VSCode
- ES7 React/Redux/GraphQL/React-native snippets
- GraphQL Language Support
- Path Intelligence

# Fork/Clone project

# Install .Net Core SDK

- Installer den nyeste .Net Core SDK: https://www.microsoft.com/net/download 

# Docker
For at køre projektet skal du have sat Databasen op. Vi vil gøre brug af Docker til at håndtere vores MSSQL Server.

Hent den nyeste version af Docker fra: https://www.docker.com/products/docker-desktop.
Installér den, om nødvendigt lav en bruger.

Kør Applikationen Docker, og vent på den er startet op.
I preferences/advanced skru RAM bruget op på 4 GB (Dette er en god idé at gøre i forhold til en MSSQL server)

For at sætte serveren op kør følgende kommando, 
For OSX: 

- $ docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -p 1433:1433 -d microsoft/mssql-server-linux:2017-latest
For Windows: 

- docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=yourStrong(!)Password" -p 1433:1433 -d microsoft/mssql-server-linux:2017-latest

Vent på at den har installeret MSSQL image i din Docker. Når den har færdiggjort kan du tjekke om serveren kører ved følgende kommando: $ docker ps

Gode kommandoer Docker:

- docker ps (Viser hvilke images, som kører lige nu)
- docker ps -a (Viser alle images som kører og dem som ikke kører)
- docker start *** (Starter et bestemt docker image, hvis ikke det kører. *** er de tre første karakterer i images navn)
- docker stop *** (Stopper et bestemt docker images. *** er de tre første karakterer i images navn)

# Database
Når serveren er oppe at køre så kan du oprette forbindelse til Serveren, og skabe Databasen.
Jeg har valgt at bruge Database IDE'en: Azure Data Studio (https://docs.microsoft.com/da-dk/sql/azure-data-studio/download?view=sql-server-2017), men du kan bruge den du har lyst til. 

For at oprette forbindelse til serveren.

- Connection type: Microsoft SQL Server
- Server: localhost
- Authentication type: SQL Login
(Dette står alt sammen i Docker image kommandoen vi kørte tidligere)
- User name: sa
- Password: yourStrong(!)Password

Når du har oprettet forbindelse til Serveren.
Så skal du køre følgende kommando i SQL for at oprette RnD Databasen: 

- CREATE DATABASE RnD

Når du har oprettet Databasen så skal du sørger for at du bruger Databasen: 

- USE RnD

I fatweekday/src/SQLQueries vil du finde alle queries, som er brugt til at oprette Schemas og Tabeller i projektet.

Start query filen: 

- Create Schema and IMPACTUser and Table Users.sql
Dernæst query filen: 

- Create Schema Pitch and Tables.sql

For at slette tabellerne og schemas query: Drop Tables and Schemas.sql

# Connect Repo and LocalServer
I filen fatweekday/src/fatweekday.Infrastructure/PitchRepository og UserRepository
Sørg for at ConnectionString'ens User og Password matcher de credentials du gav den da du lavede dit Docker Image.


# Connect Repo Azure Server
Alternativ kan du bruge ConnectionString til Azure serveren. Den kan findes i src/fatweekday.API/appsettings.json

# Build Project
I fatweekday/src/fatweekday.API kør følgende kommandoer:

- dotnet restore
- npm run build:prod

I fatweekday/src/fatweekday.Web kør følgende kommandoer:

- npm run build:prod
- npm run build:dev

# Run Project
For at køre projektet først skal du køre API'et. Naviger src/fatweekday.API og kør kommandoen: 

- $ dotnet run
Dernæst naviger ind i src/fatweekday.Web og kør kommandoen: 

- $ dotnet run

SÅDAN!
OBS: Overvej om du gerne vil have NugetPackage DotnetWatch i så fald installer den :-)

# Queries and Variables
For at finde eksempler på GraphQL Queries og Varibles kan du enten kigge i src/fatweekday.Web/app/GraphQLStatements/GraphQLStatements
eller åben filen GraphQL Queries and Variables.txt som ligger i src/fatweekday. 

# Run Newman CLI Testing (Endpoints)
For at teste alle GraphQL Endpoints skal du bruge Newman CLI.

Newman installeres global ved følgende kommando: 

- $ npm install -g newman

Når Newman er installeret. Kan endpoint testen køres. Kør filen "RnD Test Collection.json" med følgende kommando: 

- $ newman run mycollection.json
