using System.Text.RegularExpressions;
using fatweekday.Domain.Models;

namespace fatweekday.Infrastructure
{
    public static class InputValidator
    {
        const string ValidationString = @"^.{5,}$";
		const string ValidtationEffort = @"^[1-9][0-9]*$";
		const string ValidationEmail = @"[\w0-9\.-]*@impact\.dk";
        const string ValidtationPassword = @""; //TODO: Find good Regex  

        public enum InputError 
        {
            OK = 1,
            InvalidPitchTitle = 2,
            InvalidPitchDescription = 3,
            InvalidPitchEffort = 4,
            InvalidComment = 5,
            InvalidEmail = 6,
            InvalidPassword = 7,
        }

        //Checks if a string matches a Regex pattern.
        private static bool IsMatch(string Input, string RegexString)
        {
            return Regex.IsMatch(Input, RegexString, RegexOptions.IgnoreCase);
        }
        public static InputError IsPitchInputValid(Pitch Pitch)
        {	
            //Validate the Pitch input title.		
            if (!IsMatch(Pitch.Title, ValidationString))
                return InputError.InvalidPitchTitle;
            //Validate the Pitch input description.
            if (!IsMatch(Pitch.Description, ValidationString))
                return InputError.InvalidPitchDescription;
            //Validate the Pitch input effort.
            if (!IsMatch(Pitch.Effort.ToString(), ValidtationEffort))
                return InputError.InvalidPitchEffort;
                
            return InputError.OK;
        }

        public static InputError IsCommentInputValid(Comment Comment)
        {
            //Validate the Comment input text.
            if (!IsMatch(Comment.Text, ValidationString))
                return InputError.InvalidComment;

            return InputError.OK;
        }

        public static InputError IsUserInputValid(User User)
        {
            //Validate the User input email.
            if (!IsMatch(User.Email, ValidationEmail))
                return InputError.InvalidEmail;
            //Validate the User input password.
            if (!IsMatch(User.Password, ValidtationPassword))
                return InputError.InvalidPassword;

            return InputError.OK;
        }

        public static InputError IsPasswordCorrect(string InputPassword, string HashedPasswordDB)
        {
            if (!BCrypt.Net.BCrypt.Verify(InputPassword, HashedPasswordDB, false) == true)
                return InputError.InvalidPassword;
            
            return InputError.OK;
        }
    }
}