﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using fatweekday.Domain.Interfaces;
using fatweekday.Domain.Models;
namespace fatweekday.Infrastructure
{
    public class PitchRepository : IPitchRepository
    {	
		const string ConnectionString = "Server=tcp:rndmssqlserver.database.windows.net,1433;Initial Catalog=RnD;Persist Security Info=False;User ID=user;Password=SNdeM58Fnfjq ;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
		public IList<Pitch> GetAllPitches()
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}

					var Pitches = db.Query<Pitch>("SELECT * FROM Pitch.Pitches");
                    var Comments = db.Query<Comment>("SELECT * FROM Pitch.Comments");
					var PitchLikes = db.Query<PitchLike>("SELECT * FROM Pitch.PitchLikes");
					var CommentLikes = db.Query<CommentLike>("SELECT * FROM Pitch.CommentLikes");
					
						foreach(var Pitch in Pitches) {
							foreach(var Comment in Comments) {
								if(Pitch.ID == Comment.PitchID) 
								{
									Pitch.Comments.Add(Comment);
								}
								foreach(var CommentLike in CommentLikes) {
									if (Comment.ID == CommentLike.CommentID)
									{
										Comment.Likes.Add(CommentLike);
									}
								}
							}
                        	foreach(var PitchLike in PitchLikes) {
								if(Pitch.ID == PitchLike.PitchID) 
								{
									Pitch.Likes.Add(PitchLike);
								}
							}
						}
					return Pitches.ToList();
				}
		}
		public IList<PitchLike> GetAllPitchLikes()
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var PitchLikes = db.Query<PitchLike>("SELECT * FROM Pitch.PitchLikes");
					return PitchLikes.ToList();
				}
		}
		public IList<CommentLike> GetAllCommentLikes()
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var CommentLikes = db.Query<CommentLike>("SELECT * FROM Pitch.CommentLikes");

					return CommentLikes.ToList();
				}
		}
		public IList<Comment> GetAllComments()
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var Comments = db.Query<Comment>("SELECT * FROM Pitch.Comments");
					var Likes = db.Query<CommentLike>("SELECT * FROM Pitch.CommentLikes");

					foreach(var Comment in Comments) {
						foreach(var Like in Likes) {
							if(Comment.ID == Like.CommentID) {
								Comment.Likes.Add(Like);
							}
						}
					}

					return Comments.ToList();
				}
		}
		public Pitch GetPitchByID(int ID)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var PitchQuery = "SELECT * FROM Pitch.Pitches WHERE ID = @ID;";	
					var Pitch = db.Query<Pitch>(PitchQuery, new {ID}).FirstOrDefault();
					
					var PitchLikesQuery = "SELECT * FROM Pitch.PitchLikes WHERE PitchID = @ID";
					var PitchLikes = db.Query<PitchLike>(PitchLikesQuery, new {ID}).AsList();
					
					var CommentsQuery = "SELECT * FROM Pitch.Comments WHERE PitchID = @ID";
					var Comments = db.Query<Comment>(CommentsQuery, new {ID}).AsList();

					var CommentLikesQuery = "SELECT * FROM Pitch.CommentLIkes";
					var CommentLikes = db.Query<CommentLike>(CommentLikesQuery).AsList();

					foreach(var Like in PitchLikes) {
						Pitch.Likes.Add(Like);
					}
					foreach(var Comment in Comments) {
						Pitch.Comments.Add(Comment);
						foreach(var Like in CommentLikes) {
							if(Comment.ID == Like.CommentID) {
								Comment.Likes.Add(Like);
							}
						}
					}
					return Pitch;
				}
		}
		public IList<Pitch> GetPitchesByApprovedStatus(int Status) 
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					} 
					if (Status == 2) {
						return GetAllPitches();
					}
					else {
						var PitchesQuery = "SELECT * FROM Pitch.Pitches WHERE IsApproved = @Status";
						var Pitches = db.Query<Pitch>(PitchesQuery, new {Status});
						var PitchLikes = db.Query<PitchLike>("SELECT * FROM Pitch.PitchLikes");
						var Comments = db.Query<Comment>("SELECT * FROM Pitch.Comments");
						foreach(var Pitch in Pitches) {
							foreach(var Like in PitchLikes) {
								if(Pitch.ID == Like.PitchID) {
									Pitch.Likes.Add(Like);
								}
							}
							foreach(var Comment in Comments) {
								if(Pitch.ID == Comment.ID) {
									Pitch.Comments.Add(Comment);
								}
							}
						}
					return Pitches.ToList();
					}; 
				}
		}
		public IList<Comment> GetCommentsByPitchID(int ID)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var CommentsQuery = "SELECT * FROM Pitch.Comments WHERE PitchID = @ID";
					var Comments = db.Query<Comment>(CommentsQuery, new {ID});
					var CommentLikes = db.Query<CommentLike>("SELECT * FROM Pitch.CommentLikes");
					foreach(var Comment in Comments) {
						foreach(var Like in CommentLikes) {
							if (Comment.ID == Like.CommentID) {
								Comment.Likes.Add(Like);
							}
						}
					}
					return Comments.ToList();
				}
		}
		public Pitch AddPitch(Pitch Pitch) {

			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var SqlInsertQuery = "INSERT INTO Pitch.Pitches (UserID, Title, Description, Effort, IsApproved, Author) VALUES (@UserID, @Title, @Description, @Effort, @IsApproved, @Author)";
					db.Query<Pitch>(SqlInsertQuery, new {Pitch.UserID, Pitch.Title, Pitch.Description, Pitch.Effort, Pitch.IsApproved, Pitch.Author});
					
					var SqlSelectQuery = "SELECT * FROM Pitch.Pitches WHERE UserID = @UserID AND Title = @Title";
					return db.Query<Pitch>(SqlSelectQuery, new {Pitch.UserID, Pitch.Title}).FirstOrDefault();
				}
		}
		public PitchLike AddLikeToPitch(PitchLike Like) 
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
			{
				if (db.State == ConnectionState.Closed)
				{
					db.Open();
				}
				var SqlInsertQuery ="INSERT INTO Pitch.PitchLikes (UserID, PitchID) VALUES (@UserID, @PitchID)";
				db.Query<PitchLike>(SqlInsertQuery, new {Like.UserID, Like.PitchID}).FirstOrDefault();

				var SqlSelectQuery = "SELECT * FROM Pitch.PitchLikes WHERE UserID = @UserID AND PitchID = @PitchID";
				return db.Query<PitchLike>(SqlSelectQuery, new {Like.UserID, Like.PitchID}).FirstOrDefault();
						
			}
		}
		public CommentLike AddLikeToComment(CommentLike Like) 
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
			{
				if (db.State == ConnectionState.Closed)
				{
					db.Open();
				}

				var sqlQueryInsertPitch ="INSERT INTO Pitch.CommentLikes (UserID, CommentID) VALUES (@UserID, @CommentID)";
				db.Query<CommentLike>(sqlQueryInsertPitch, new {Like.UserID, Like.CommentID}).FirstOrDefault();

				var SqlSelectQuery = "SELECT * FROM Pitch.CommentLikes WHERE UserID = @UserID AND CommentID = @CommentID";
				return db.Query<CommentLike>(SqlSelectQuery, new {Like.UserID, Like.CommentID}).FirstOrDefault();
						
			}
		}
		public PitchLike RemoveLikeFromPitch(PitchLike Like)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
			{
				if (db.State == ConnectionState.Closed)
				{
					db.Open();
				}
				var SqlSelectQuery = "SELECT * FROM Pitch.PitchLikes WHERE PitchID = @PitchID AND UserID = @UserID";
				var RemovedPitchLike =  db.Query<PitchLike>(SqlSelectQuery, new {Like.PitchID, Like.UserID}).FirstOrDefault();
				
				var SqlDeleteQuery = "DELETE FROM Pitch.PitchLikes WHERE PitchID = @PitchID AND UserID = @UserID";
				db.Query<PitchLike>(SqlDeleteQuery, new {Like.PitchID, Like.UserID}).FirstOrDefault();
				
				return RemovedPitchLike;
			}	
		}
		public CommentLike RemoveLikeFromComment(CommentLike Like)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
			{
				if (db.State == ConnectionState.Closed)
				{
					db.Open();
				}
				var SqlSelectQuery = "SELECT * FROM Pitch.CommentLikes WHERE CommentID = @CommentID AND UserID = @UserID";
				var RemovedCommentLike = db.Query<CommentLike>(SqlSelectQuery, new {Like.CommentID, Like.UserID}).FirstOrDefault();

				var SqlDeleteQuery = "DELETE FROM Pitch.CommentLikes WHERE CommentID = @CommentID AND UserID = @UserID";
				db.Query<CommentLike>(SqlDeleteQuery, new {Like.CommentID, Like.UserID}).FirstOrDefault();
				return RemovedCommentLike;
			}	
		}
		public Comment AddComment(Comment Comment) {
			
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var SqlInsertQuery = "INSERT INTO Pitch.Comments (UserID, PitchID, Text, Author) VALUES (@UserID, @PitchID, @Text, @Author)";
					db.Query<Comment>(SqlInsertQuery, new {Comment.UserID, Comment.PitchID, Comment.Text, Comment.Author}).FirstOrDefault();

					var SqlSelectQuery = "SELECT * FROM Pitch.Comments WHERE UserID = @UserID AND PitchID = PitchID AND Text = @Text";
					return db.Query<Comment>(SqlSelectQuery, new {Comment.UserID, Comment.PitchID, Comment.Text}).FirstOrDefault();
				}
		}
		public Pitch ApprovePitch(int PitchID) {

			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var SqlUpdateQuery = "UPDATE Pitch.Pitches SET IsApproved = 1 WHERE ID = @PitchID";
					db.Query<Pitch>(SqlUpdateQuery, new {PitchID}).FirstOrDefault();	

					var SqlSelectQuery = "SELECT * FROM Pitch.Pitches WHERE ID = @PitchID";
					return db.Query<Pitch>(SqlSelectQuery, new {PitchID}).FirstOrDefault();	
				}
		}
		public Pitch UnApprovePitch(int PitchID)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var SqlUpdateQuery = "UPDATE Pitch.Pitches SET IsApproved = 0 WHERE ID = @PitchID";
					db.Query<Pitch>(SqlUpdateQuery, new {PitchID}).FirstOrDefault();	

					var SqlSelectQuery = "SELECT * FROM Pitch.Pitches WHERE ID = @PitchID";
					return db.Query<Pitch>(SqlSelectQuery, new {PitchID}).FirstOrDefault();	
				}
		}
		public Boolean DoesSaltExist(string Salt)
        {
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var SqlQuery = "SELECT Salt FROM IMPACTUser.Users WHERE Salt = @Salt";
					if (db.Query<User>(SqlQuery, new {Salt}).FirstOrDefault() != null) {
						return true;
					}
					else {
						return false;
					}
				}
        }
		public Boolean DoesPitchExist(int PitchID)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					System.Console.WriteLine("PitchID: " + PitchID);
				    var SqlQuery = "SELECT * FROM Pitch.Pitches WHERE ID = @PitchID";
					var Pitch = db.Query<Pitch>(SqlQuery, new {PitchID}).FirstOrDefault();
					
				    if (Pitch != null)
				    {
						return true;
					}
					else {
						return false;
					}
				}
		}
		public Boolean DoesLikeOnPitchExist(int UserID, int PitchID)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString)) {
				if (db.State == ConnectionState.Closed)
				{
					db.Open();
				}
				var SqlQuery = "SELECT ID FROM Pitch.PitchLikes WHERE UserID = @UserID AND PitchID = @PitchID";
				var Pitch = db.Query<PitchLike>(SqlQuery, new {UserID, PitchID}).FirstOrDefault();
				if (Pitch != null) {
					return true;
				}
				else {
					return false;
				}
			}

		}
		public Boolean DoesLikeOnCommentExist(int UserID, int CommentID)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString)) 
			{
				if (db.State == ConnectionState.Closed)
				{
					db.Open();
				}
				var SqlQuery = "SELECT ID FROM Pitch.CommentLikes WHERE UserID = @UserID AND CommentID = @CommentID";
				var Comment = db.Query<CommentLike>(SqlQuery, new {UserID, CommentID}).FirstOrDefault();
				if (Comment != null) {
					return true;
				}
				else {
					return false;
				}
			}
		}
	}
}
