﻿using JWT.Algorithms;
using JWT.Builder;
using System;
using JWT;

namespace fatweekday.Infrastructure
{
    public class JWTBuilder
    {
        private const string secret = "P*KFtkKp&mUcJn?RWSB-Xqu8Udc3_tEk?uEaarrcBUkbLp$rmKWLzdZ&djPkB52385T@vHv=Xj=2nr+6!mgGBu@Ggp2cCyaC+?W*uaLm#bFb#7SR%+*q25+#fF_MuLKU";

        public bool VerifyToken(string token)
        {
            string json;
            try
            {
                json = new JwtBuilder().WithSecret(secret).MustVerifySignature().Decode(token);

            }
            catch (TokenExpiredException)
            {
                return false;
            }
            catch (SignatureVerificationException)
            {
                return false;
            }
            return true;
        }
        public static string CreateToken(int UserID, int IsAdmin, string Email)
        {  
            var token = new JwtBuilder().WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(secret)
                .AddClaim("userId", UserID)
                .AddClaim("email", Email)
                .AddClaim("isAdmin", IsAdmin)
                .AddClaim("exp",DateTimeOffset.UtcNow.AddDays(7.0).ToUnixTimeSeconds())
                .Build();  
            return token;
        }      
    }
}
