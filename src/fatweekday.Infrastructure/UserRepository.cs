using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using fatweekday.Domain.Interfaces;
using fatweekday.Domain.Models;

namespace fatweekday.Infrastructure
{
    public class UserRepository : IUserRepository
    {
        const string ConnectionString = "Server=tcp:rndmssqlserver.database.windows.net,1433;Initial Catalog=RnD;Persist Security Info=False;User ID=user;Password=SNdeM58Fnfjq ;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30";

        public User AddUser(User User)
		{
			try {
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var PitchRepository = new PitchRepository();
					string[] EmailStringSplits = User.Email.Split('@');
					var Initials = EmailStringSplits[0].ToString();
					int IsAdmin = 0;
				    string Salt;
					do
				    {
				        Salt = BCrypt.Net.BCrypt.GenerateSalt(7);
                    }
                    while(PitchRepository.DoesSaltExist(Salt) == true);
					
					string PasswordHash = BCrypt.Net.BCrypt.HashPassword(User.Password, Salt);
					
					try {
					var SqlInsertQuery = "INSERT INTO IMPACTUser.Users (Initials, Email, IsAdmin, Password, Salt) VALUES (@Initials, @Email, @IsAdmin, @PasswordHash, @Salt)";			 
					return db.Query<User>(SqlInsertQuery, new {Initials, User.Email, IsAdmin, PasswordHash, Salt}).FirstOrDefault();
					}
					catch (Exception e) {
						System.Console.WriteLine(e);
						return null;
					}
			} 
			}
			catch (Exception e) {
				System.Console.WriteLine(e);
				return null;
			}
		}

        public Boolean DoesEmailExist(string Email)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
                    //Check if email is in use
				    var sqlQuery = "SELECT Email FROM IMPACTUser.Users WHERE Email = @Email";
				    if (db.Query<User>(sqlQuery, new {Email}).FirstOrDefault() != null)
				    {
						return true;
					}
					else {
						return false;
					}
				}
		}

        public Boolean DoesUserExistByEmail(string Email)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var sqlQuery = "SELECT ID FROM IMPACTUser.Users WHERE Email = @Email";
					if(db.Query<User>(sqlQuery, new {Email}).FirstOrDefault() != null)
					{
						return true;
					}
					else 
					{
						return false;
					}
				}
		}

		public Boolean DoesUserExist(int ID)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var sqlQuery = "SELECT ID FROM IMPACTUser.Users WHERE ID = @ID";
					if(db.Query<User>(sqlQuery, new {ID}).FirstOrDefault() != null)
					{
						return true;
					}
					else 
					{
						return false;
					}
				}
		}

        public IList<User> GetAllUsers()
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
				{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var Users = db.Query<User>("SELECT * FROM IMPACTUser.Users");
					var Pitches = db.Query<Pitch>("SELECT * FROM Pitch.Pitches");
                    var Comments = db.Query<Comment>("SELECT * FROM Pitch.Comments");
					var PitchLikes = db.Query<PitchLike>("SELECT * FROM Pitch.PitchLikes");
                    var CommentLikes = db.Query<CommentLike>("SELECT * FROM Pitch.CommentLikes");
					
					foreach(var User in Users) {
						foreach(var Pitch in Pitches) {
							if(User.ID == Pitch.UserID) {
								User.Pitches.Add(Pitch);
							}
						}
						foreach(var Comment in Comments) {
							if(User.ID == Comment.UserID) {
								User.Comments.Add(Comment);
							}
						}
                        foreach(var PitchLike in PitchLikes) {
							if(User.ID == PitchLike.UserID) {
								User.PitchLikes.Add(PitchLike);
							}
						}
                        foreach(var CommentLike in CommentLikes) {
							if(User.ID == CommentLike.UserID) {
								User.CommentLikes.Add(CommentLike);
							}
						}
					}
					return Users.ToList();
				}
		}

        public User GetUserByID(int ID)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
			{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var SqlSelectUser = "SELECT * FROM IMPACTUser.Users WHERE ID = @ID";
					var User = db.Query<User>(SqlSelectUser, new {ID}).FirstOrDefault();
					var Pitches = db.Query<Pitch>("SELECT * FROM Pitch.Pitches");
                    var Comments = db.Query<Comment>("SELECT * FROM Pitch.Comments");
					var PitchLikes = db.Query<PitchLike>("SELECT * FROM Pitch.PitchLikes");
                    var CommentLikes = db.Query<CommentLike>("SELECT * FROM Pitch.CommentLikes");

						foreach(var Pitch in Pitches) {
							if(ID == Pitch.UserID) {
								User.Pitches.Add(Pitch);
							}
						}
						foreach(var Comment in Comments) {
							if(ID == Comment.UserID) {
								User.Comments.Add(Comment);
							}
						}
                        foreach(var PitchLike in PitchLikes) {
							if(ID == PitchLike.UserID) {
								User.PitchLikes.Add(PitchLike);
							}
						}
                        foreach(var CommentLike in CommentLikes) {
							if(ID == CommentLike.UserID) {
								User.CommentLikes.Add(CommentLike);
							}
						}
					
					return User;
			}
		}

        public User GetUserIDByEmail(string Email)
		{
			using(IDbConnection db = new SqlConnection(ConnectionString))
			{
					if (db.State == ConnectionState.Closed)
					{
						db.Open();
					}
					var sqlQuery = "SELECT * FROM IMPACTUser.Users WHERE Email = @Email";
					return db.Query<User>(sqlQuery, new {Email}).FirstOrDefault();
			}
		}
    }
}


