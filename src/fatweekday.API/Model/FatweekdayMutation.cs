﻿using fatweekday.API.Model.InputTypes;
using fatweekday.API.Model.Types;
using fatweekday.BL.Services;
using fatweekday.Domain.Interfaces;
using fatweekday.Domain.Models;
using fatweekday.Infrastructure;
using GraphQL;
using GraphQL.Types;

namespace fatweekday.API.Model
{
    public class FatweekdayMutation : ObjectGraphType
	{
		public FatweekdayMutation(IPitchRepository PitchRepo, IUserRepository UserRepo)
		{
			Name = "Mutation";
			Description = "All Mutations within the project";

			Field<PitchType>(
				"AddPitch", "Adds a Pitch",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<PitchInputType>> { Name = "Pitch", Description = "The Pitch you want to create" }
				),
				resolve: context =>
				{
					var Pitch = context.GetArgument<Pitch>("Pitch");
					// Checks if the Input is valid
					var ValidationResponse = InputValidator.IsPitchInputValid(Pitch);
					if (ValidationResponse != InputValidator.InputError.OK)
					{
					 	context.Errors.Add(new ExecutionError("Error: " + ValidationResponse));
					 	return null;
					}
					// Adds notificationservice, that sends a new Pitch notification
					var NotificationService = new NotificationService();
					NotificationService.newPitchNotification(Pitch);

					return PitchRepo.AddPitch(Pitch);
				});

			Field<PitchLikeType>(
				"AddLikePitch", "Adds a Like a to Pitch",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<PitchLikeInputType>> { Name = "PitchLike", Description = "The Pitch Like you want to add" }
				),
				resolve: context =>
				{
					var Like = context.GetArgument<PitchLike>("PitchLike");

					// Checks if the User has already liked the Pitch
					if(PitchRepo.DoesLikeOnPitchExist(Like.UserID, Like.PitchID))
					{
						context.Errors.Add(new ExecutionError("You can't Like a Pitch more than once"));
						return null; 
					}
					return PitchRepo.AddLikeToPitch(Like);
				});

			Field<CommentLikeType>(
				"AddLikeComment", "Adds a Like to a Comment",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<CommentLikeInputType>> { Name = "CommentLike", Description = "The Comment Like you want to add" }
				),
				resolve: context =>
				{
					var Like = context.GetArgument<CommentLike>("CommentLike");

					// Checks if the User has already liked the Comment
					if(PitchRepo.DoesLikeOnCommentExist(Like.UserID, Like.CommentID))
					{
						context.Errors.Add(new ExecutionError("You can't Like a Comment more than once"));
						return null; 
					}
					
					return PitchRepo.AddLikeToComment(Like);
				});

			Field<PitchLikeType>(
				"RemoveLikeFromPitch", "Removes a Like from a Pitch",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<PitchLikeInputType>> { Name = "PitchLike", Description = "The PitchLike you want to remove" }
				),
				resolve: context =>
				{
					var Like = context.GetArgument<PitchLike>("PitchLike");
					// Checks if the User has liked the Pitch
					if(!PitchRepo.DoesLikeOnPitchExist(Like.UserID, Like.PitchID))
					{
						context.Errors.Add(new ExecutionError("You can't remove a Like from a Pitch that doesn't exist"));
						return null; 
					}
					return PitchRepo.RemoveLikeFromPitch(Like);
				});

			Field<CommentLikeType>(
				"RemoveLikeFromComment", "Removes a Like from a Comment",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<CommentLikeInputType>> { Name = "CommentLike", Description = "The CommentLike you want to remove" }
				),
				resolve: context =>
				{
					var Like = context.GetArgument<CommentLike>("CommentLike");
					// Checks if the User has liked the Comment
					if(!PitchRepo.DoesLikeOnCommentExist(Like.UserID, Like.CommentID))
					{
						context.Errors.Add(new ExecutionError("You can't remove a Like from a Comment that doesn't exist"));
						return null; 
					}
					return PitchRepo.RemoveLikeFromComment(Like);
				});

			Field<CommentType>(
				"AddComment", "Adds a Comment", 
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<CommentInputType>> { Name = "Comment", Description = "The Comment you want to add" }
				),
				resolve: context =>
				{
					var Comment = context.GetArgument<Comment>("Comment");
					// Checks if the Input is valid
					var ValidationResponse = InputValidator.IsCommentInputValid(Comment);
					if(ValidationResponse != InputValidator.InputError.OK)
					{
						context.Errors.Add(new ExecutionError("Error: " + ValidationResponse));
						return null;
					}
					// Checks if the Pitch exists
					if(!PitchRepo.DoesPitchExist(Comment.PitchID))
					{
						context.Errors.Add(new ExecutionError("Pitch doesn't exist"));
						return null;
					}
					// Checks if the User exists
					if(!UserRepo.DoesUserExist(Comment.UserID))
					{
						context.Errors.Add(new ExecutionError("User doesn't exist"));
						return null;
					}
					return PitchRepo.AddComment(Comment);
				});

			Field<PitchType>(
				"ApprovePitch", "Approves a Pitch",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "ID", Description = "The ID of the Pitch you want to approve" }
				),
				resolve: context =>
				{
					var PitchID = context.GetArgument<int>("ID");

					// Checks if the Pitch exists
					if(!PitchRepo.DoesPitchExist(PitchID))
					{
						context.Errors.Add(new ExecutionError("Pitch doesn't exist"));
						return null;
					}
					// Adds notificationservice, that sends a notification about the Pitch that was approved
					var NotificationService = new NotificationService();
					NotificationService.pitchApprovedNotification(PitchRepo.GetPitchByID(PitchID));

					return PitchRepo.ApprovePitch(PitchID);

				});

			Field<PitchType>(
				"UnApprovePitch", "UnApproves a Pitch",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "ID", Description = "The ID of the Pitch you want to unapprove" }
				),
				resolve: context =>
				{
					var PitchID = context.GetArgument<int>("ID");
					
					// Checks if the Pitch exists
					if(!PitchRepo.DoesPitchExist(PitchID))
					{
						context.Errors.Add(new ExecutionError("Pitch doesn't exist"));
						return null;
					}

					return PitchRepo.UnApprovePitch(PitchID);

				});	

			Field<UserType>(
				"Register", "Adds a User",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<UserInputType>> { Name = "User", Description = "The User you want to register" }
				),
				resolve: context =>
				{
					var User = context.GetArgument<User>("User");
					
					// Checks if the Input is valid
					var ValidationResponse = InputValidator.IsUserInputValid(User);
					if(ValidationResponse != InputValidator.InputError.OK)
					{
						context.Errors.Add(new ExecutionError("Error: " + ValidationResponse));
						return null;
					}
					// Checks if the Email exists
					if (UserRepo.DoesEmailExist(User.Email))
					{
						context.Errors.Add(new ExecutionError("Email already exists"));
						return null;
					}
					// Adds the User to the DB 
					UserRepo.AddUser(User);

					// Gets the User from the DB with the ID
					var DBuser = UserRepo.GetUserIDByEmail(User.Email);

					// Builds the User object from the Database response, and sends the User object with a JWT as a response
					User.JWT = JWTBuilder.CreateToken(DBuser.ID, User.IsAdmin, User.Email);

					return User;
				});

			Field<UserType>(
				"Login", "Returns the User object if the Login credentials are true",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<UserInputType>> { Name = "User", Description = "The User you want to log in"}
				),
				resolve: context => 
				{
					var User = context.GetArgument<User>("User");
					
					// Checks if the Input is valid
					var ValidationResponse = InputValidator.IsUserInputValid(User);
					if(ValidationResponse != InputValidator.InputError.OK)
					{
						context.Errors.Add(new ExecutionError("Error: " + ValidationResponse));
						return null;
					}
					// Checks if the Email exists
					if(!UserRepo.DoesUserExistByEmail(User.Email))
					{
						context.Errors.Add(new ExecutionError("User doesn't exist"));
						return null;
					}
					// Gets the User from the DB with the ID
					var DBuser = UserRepo.GetUserIDByEmail(User.Email);
					
					// Validates the DB Password with the Input password
					var ValidationResponsePassword = InputValidator.IsPasswordCorrect(User.Password, DBuser.Password);
					if(ValidationResponsePassword != InputValidator.InputError.OK)
					{
						context.Errors.Add(new ExecutionError("Error: " + ValidationResponsePassword));
						return null;
					}
					// Builds the User object from the Database response, and sends the User object with a JWT as a response
					User.JWT = JWTBuilder.CreateToken(DBuser.ID, DBuser.IsAdmin, User.Email);

					return User;
				}
			);
		}
	}
}
