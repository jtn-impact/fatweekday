﻿using fatweekday.API.Model.Types;
using fatweekday.Domain.Interfaces;
using GraphQL.Types;

namespace fatweekday.API.Model
{
    public class FatweekdayQuery : ObjectGraphType<object>
	{
		public FatweekdayQuery(IPitchRepository PitchRepo, IUserRepository UserRepo)
		{
			Name = "Query";
			Description = "All Queries within the project";

			Field<ListGraphType<PitchType>>(
				"AllPitches", "Gets all the Pitches and all the Pitches Fields",
				resolve: context => PitchRepo.GetAllPitches()
			);

			Field<ListGraphType<UserType>>(
				"AllUsers", "Gets all the Users and all the Users Fields",
				resolve: context => UserRepo.GetAllUsers()
			);

			Field<ListGraphType<CommentType>>(
				"AllComments", "Gets all the Comments and all the Comments Fields",
				resolve: context => PitchRepo.GetAllComments()
			);

			Field<ListGraphType<PitchLikeType>>(
				"AllPitchLikes", "Gets all the Pitchlikes",
				resolve: context => PitchRepo.GetAllPitchLikes()
			);

			Field<ListGraphType<CommentLikeType>>(
				"AllCommentLikes", "Gets all the CommentLikes",
				resolve: context => PitchRepo.GetAllCommentLikes()
			);

			Field<PitchType>(
				"PitchByID", "Gets specific Pitch by ID, and returb it and all the Fields",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "ID", Description = "The ID of the Pitch" }
				),
				resolve: context => PitchRepo.GetPitchByID(context.GetArgument<int>("ID"))
			);

			Field<ListGraphType<PitchType>>(
				"PitchesByApprovedStatus", "Gets all Pitches by their Status given in the argument, and returns all the Pitches with that status, and their Fields.",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "Approved", Description = "Approved status of the pitch you want to get" }
				),
				resolve: context => PitchRepo.GetPitchesByApprovedStatus(context.GetArgument<int>("Approved"))
			);

			Field<ListGraphType<CommentType>>(
				"CommentsByPitchID", "Gets all the Comments from a Pitch specified by ID",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "ID", Description = "ID of the Pitch you want to get comments from" }
				),
				resolve: context => PitchRepo.GetCommentsByPitchID(context.GetArgument<int>("ID"))
			);

			Field<UserType>(
				"UserByID", "Gets the User specicfied by ID, and return Fields on User, but not Sub-Fields",
				arguments:new QueryArguments(
					new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "ID", Description = "The ID of the User you want to get" }
				),
				resolve: context => UserRepo.GetUserByID(context.GetArgument<int>("ID"))
			);
		}
	}
}
