using fatweekday.Domain.Interfaces;
using GraphQL.Types;

namespace fatweekday.API.Model
{
    public class FatweekdaySubscription : ObjectGraphType
	{
		public FatweekdaySubscription(IPitchRepository repo)
		{
			Name = "Subscription";
			Description = "Subscriptions within the project";

		}
	}
}
