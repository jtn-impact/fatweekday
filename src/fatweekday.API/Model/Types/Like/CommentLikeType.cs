﻿using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.Types
{
    public class CommentLikeType : ObjectGraphType<CommentLike>
	{
		public CommentLikeType() {
			
			Name = "CommentLike";
			Field(h => h.ID, nullable: true).Description("The ID of the Like");
			Field(h => h.CommentID).Description("The Pitch ID");
			Field(h => h.UserID).Description("The User ID");
			Field(h => h.CreationDate, nullable: true).Description("Date of creation");
		}
    }
}
