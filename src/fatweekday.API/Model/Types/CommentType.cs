using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.Types
{
    public class CommentType : ObjectGraphType<Comment>
	{
		public CommentType() {
			
			Name = "Comment";
			Field(h => h.ID, nullable: true).Description("The ID of the Comment");
			Field(h => h.PitchID).Description("The Pitch ID");
			Field(h => h.UserID).Description("The User ID");
			Field(h => h.CreationDate, nullable: true).Description("Date of creation");
            Field(h => h.Text).Description("Comment text");
			Field(h => h.Author).Description("Author of the comment");

			//Objects
			Field<ListGraphType<CommentLikeType>>(
				"Likes",
				resolve: context => context.Source.Likes
			);
			
			//Counters
			Field<IntGraphType>(
				"LikeCount",
				resolve: context => context.Source.Likes.Count
			);
			
		}
    }
}
