using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.Types
{
    public class UserType : ObjectGraphType<User>
	{
		public UserType() {
			
			Name = "User";
            Field(h => h.ID, nullable: true).Description("The ID of the user");
            Field(h => h.IsAdmin).Description("Is the user an Admin");
            Field(h => h.Email).Description("The email of the user");
            Field(h => h.Password).Description("The password of the user");
            Field(h => h.Initials, nullable: true).Description("The initials of the user");
			Field(h => h.CreationDate, nullable: true).Description("The creationsdate of the user");
            Field(h => h.ImageURL, nullable: true).Description("Userimage URL");
			Field(h => h.JWT, nullable: true).Description("JWT token of the user");
			
			//Objects
            Field<ListGraphType<PitchType>>(
				"Pitches",
				resolve: context => context.Source.Pitches
			);
			Field<ListGraphType<PitchLikeType>>(
				"PitchLikes",
				resolve: context => context.Source.PitchLikes
			);
			Field<ListGraphType<CommentType>>(
				"Comments",
				resolve: context => context.Source.Comments
			);
			Field<ListGraphType<CommentLikeType>>(
				"CommentLikes",
				resolve: context => context.Source.CommentLikes
			);

			//Counters
            Field<IntGraphType>(
				"PitchCount",
				resolve: context => context.Source.Pitches.Count
			);
			Field<IntGraphType>(
				"CommentCount",
				resolve: context => context.Source.Comments.Count
			);
            Field<IntGraphType>(
				"PitchesLikeCount",
				resolve: context => context.Source.PitchLikes.Count
			);
			Field<IntGraphType>(
				"CommentLikeCount",
				resolve: context => context.Source.CommentLikes.Count
			);
		}
    }
}
