﻿using fatweekday.Domain.Interfaces;
using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.Types
{
    public class PitchType : ObjectGraphType<Pitch>
	{
		public PitchType() {
			
			Name = "Pitch";
			Field(h => h.ID, nullable: true).Description("The Pitch ID");
			Field(h => h.UserID).Description("The ID of the Creator");
			Field(h => h.Title).Description("The Pitch title");
			Field(h => h.Description).Description("The Pitch description");
			Field(h => h.CreationDate, nullable: true).Description("Creation time of the pitch");
			Field(h => h.Effort).Description("The Pitch effort");
			Field(h => h.IsApproved).Description("Is the pitch approved?");
			Field(h => h.Author).Description("Author of the Pitch");

			//Objects
			Field<ListGraphType<PitchLikeType>>(
				"Likes",
				resolve: context => context.Source.Likes
			);
			Field<ListGraphType<CommentType>>(
				"Comments",
				resolve: context => context.Source.Comments
			);

			//Counters
			Field<IntGraphType>(
				"LikeCount",
				resolve: context => context.Source.Likes.Count
			);
			Field<IntGraphType>(
				"CommentCount",
				resolve: context => context.Source.Comments.Count
			);

		}
    }
}
