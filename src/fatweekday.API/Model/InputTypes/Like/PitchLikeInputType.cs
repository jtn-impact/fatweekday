using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.InputTypes
{
    public class PitchLikeInputType : InputObjectGraphType<PitchLike>
	{
		public PitchLikeInputType()
		{
			Name = "PitchLikeInput";
			Field(h => h.PitchID).Description("The Pitch Id");
			Field(h => h.UserID).Description("The User Id");
			
		}
	}
}