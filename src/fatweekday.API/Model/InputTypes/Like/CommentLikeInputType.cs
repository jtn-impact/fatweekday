using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.InputTypes
{
    public class CommentLikeInputType : InputObjectGraphType<CommentLike>
	{
		public CommentLikeInputType()
		{
			Name = "CommentLikeInput";
			Field(h => h.CommentID).Description("The Comment Id");
			Field(h => h.UserID).Description("The User Id");
		}
	}
}