﻿using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.InputTypes
{
    public class PitchInputType : InputObjectGraphType<Pitch>
	{
		public PitchInputType()
		{
			Name = "PitchInput";
			Field(h => h.UserID).Description("The ID of the Creator");
			Field(h => h.Title).Description("The Pitch title");
			Field(h => h.Description).Description("The Pitch description");
			Field(h => h.Effort).Description("The Pitch effort");
			Field(h => h.Author).Description("Author of the comment");
		}
	}
}
