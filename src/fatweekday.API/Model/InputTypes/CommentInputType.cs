using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.InputTypes
{
    public class CommentInputType : InputObjectGraphType<Comment>
	{
		public CommentInputType()
		{
			Name = "CommentInput";
			Field(h => h.PitchID).Description("The Pitch ID");
			Field(h => h.UserID).Description("The User ID");
            Field(h => h.Text).Description("Comment text");
			Field(h => h.Author).Description("Author of the comment");
			
		}
	}
}