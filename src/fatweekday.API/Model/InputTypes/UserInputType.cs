using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.InputTypes
{
    public class UserInputType : InputObjectGraphType<User>
	{
		public UserInputType()
		{
			Name = "UserInput";
			Field(h => h.Email).Description("The email of the user");
            Field(h => h.Password).Description("The password of the user");
		}
	}
}
