﻿using System.Collections.Generic;

namespace fatweekday.Domain.Models
{
    public class Pitch
    {
		public int ID { get; set; }
		public int UserID { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public string CreationDate { get; set;} 
		public int Effort { get; set; }
		public int IsApproved { get; set;}
		public string Author { get; set;}
		public IList<PitchLike> Likes { get; set; }
		public IList<Comment> Comments { get; set;}
		public Pitch()
		{
			IsApproved = 0;
			Likes = new List<PitchLike>();
			Comments = new List<Comment>();
		}
    }
}
