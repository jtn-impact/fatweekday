using System.Collections.Generic;

namespace fatweekday.Domain.Models
{
    public class Comment
    {
		public int ID { get; set; }
        public string Text { get; set;}
		public string CreationDate { get; set; }
		public int PitchID { get; set; }
		public int UserID { get; set; }
		public string Author {get; set; }
		public IList<CommentLike> Likes { get; set; }
		
		//Navigation Properties
		public Pitch Pitch { get; set; }
		public User User { get; set; }
		public Comment()
		{
			Likes = new List<CommentLike>();
		}
	}
}
