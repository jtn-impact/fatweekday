using System.Collections.Generic;

namespace fatweekday.Domain.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Initials { get; set;}
        public string Email { get; set;}
        public int IsAdmin { get; set;}
        public string Salt {get; set;}
        public string Password { get; set;}
        public string CreationDate { get; set;} 
        public string ImageURL { get; set;}
        public string JWT { get; set;}
        public IList<Pitch> Pitches {get; set;}
        public IList<Comment> Comments {get; set;}
        public IList<PitchLike> PitchLikes {get; set;}
        public IList<CommentLike> CommentLikes {get; set;}

        public User()
		{
            Pitches = new List<Pitch>();
			Comments = new List<Comment>();
            PitchLikes = new List<PitchLike>();
            CommentLikes = new List<CommentLike>();
        }
	}
}
