﻿namespace fatweekday.Domain.Models
{
    public class CommentLike : LikeBase
    {

		public int CommentID { get; set; }

		//Navigation Properties
		public Comment Comment { get; set; }

	}
}
