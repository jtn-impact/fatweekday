﻿namespace fatweekday.Domain.Models
{
    public abstract class LikeBase
    {
		public int ID { get; set; }
		public string CreationDate { get; set; }
		public int UserID { get; set; }
		public User User { get; set; }
	}
}
