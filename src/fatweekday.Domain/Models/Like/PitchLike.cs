﻿namespace fatweekday.Domain.Models
{
    public class PitchLike : LikeBase
    {

		public int PitchID { get; set; }

		//Navigation Properties
		public Pitch Pitch { get; set; }

	}
}
