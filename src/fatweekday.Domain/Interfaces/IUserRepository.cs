using System;
using System.Collections.Generic;
using fatweekday.Domain.Models;

namespace fatweekday.Domain.Interfaces
{
    public interface IUserRepository
	{
        //Getters
        IList<User> GetAllUsers();
        User GetUserByID(int ID);
		User GetUserIDByEmail(string Email);
        //Adders
        User AddUser(User User);
        //Checkers
        Boolean DoesUserExist(int ID);
        Boolean DoesUserExistByEmail(string Email);
        Boolean DoesEmailExist(string Email);

    }
}