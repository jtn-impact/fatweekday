﻿using System;
using System.Collections.Generic;
using fatweekday.Domain.Models;

namespace fatweekday.Domain.Interfaces
{
    public interface IPitchRepository
	{
		//GETTERS
		IList<Pitch> GetAllPitches();
		IList<PitchLike> GetAllPitchLikes();
		IList<CommentLike> GetAllCommentLikes();
		IList<Comment> GetAllComments(); 
		Pitch GetPitchByID(int ID);
		IList<Pitch> GetPitchesByApprovedStatus(int Status);
		IList<Comment> GetCommentsByPitchID(int ID);
		//SETTERS
		Pitch AddPitch(Pitch Pitch);
		PitchLike AddLikeToPitch(PitchLike Like);
		CommentLike AddLikeToComment(CommentLike Like);
		Comment AddComment(Comment Comment);
		Pitch ApprovePitch(int PitchID);
		Pitch UnApprovePitch(int PitchID);
		//EDITERS
		PitchLike RemoveLikeFromPitch(PitchLike Like);
		CommentLike RemoveLikeFromComment(CommentLike Like);
		//CHECKERS
		Boolean DoesSaltExist(string Salt);
		Boolean DoesPitchExist(int PitchID);
		Boolean DoesLikeOnPitchExist(int UserID, int PitchID);
		Boolean DoesLikeOnCommentExist(int UserID, int CommentID);
	}
}
