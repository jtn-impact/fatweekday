using fatweekday.Domain.Models;
using fatweekday.BL.Services;
using System;
using Xunit;

namespace fatweekday.Domain.Tests
{
    public class Rating
    {
		private RatingsCalculator ratingsCalculator = new RatingsCalculator();

		[Fact]
		public void IsZeroRating()
		{
			var pitch = new Pitch() { Likes = new PitchLike[] {} };
	
			Assert.Equal(1, ratingsCalculator.GetRating(pitch));
		}
	
		//[Fact]
		// public void IsRating()
		// {
		// 	var pitch = new Pitch()
		// 	{
		// 		Likes = new Like[] {
		// 			new Like { CreationDate = DateTime.UtcNow.AddDays(-4) },
		// 			new Like { CreationDate = DateTime.UtcNow.AddDays(-5) },
		// 		}
		// 	};
	
		// 	Assert.Equal(2, ratingsCalculator.GetRating(pitch));
		// }
	
		//[Fact]
		// public void IsRatingWithBonus()
		// {
		// 	var pitch = new Pitch()
		// 	{
		// 		Likes = new Like[] {
		// 			new Like { CreationDate = DateTime.UtcNow },
		// 			new Like { CreationDate = DateTime.UtcNow.AddDays(-5) },
		// 		}
		// 	};

		// 	Assert.Equal(3, ratingsCalculator.GetRating(pitch));
		// }
	}
}
