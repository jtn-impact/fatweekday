import * as React from 'react';
import { Mutation } from 'react-apollo';
import { LOGIN } from '../../GraphQLStatements/GraphQLStatements';
import { AuthenticationService } from '../../Services/Authentication.service';
import { ValidationService } from '../../Services/Validation.service';
import ImpactButton from '../../Main/ImpactButton';
import { ILogin } from '../../Models/ILogin';

class SignIn extends React.Component<ILogin, any> {

    state = {
        isEmailActive: false,
        isPasswordActive: false,
    }

    onBlur = (value: string, stateObject: string) => {
        if (value.length != 0 ) {
            this.setState({[stateObject]: true})
        }
        else {
            this.setState({[stateObject]: false})
        }
    }

    render() {

    const { isEmailActive, isPasswordActive } = this.state;
    const { showContent, navigate } = this.props;

    let authService = new AuthenticationService();
    let validService = new ValidationService();

    let inputEmail : any;
    let inputPassword : any;
    
        return (
            <Mutation mutation={LOGIN}>
                {(Login) => (
                 <form className="signin__content" onSubmit={e => {
                    e.preventDefault();
                    if (validService.isLoginValid(inputEmail.value, inputPassword.value)) {
                        Login({
                            variables: { 
                               user: {
                               "email": inputEmail.value as String, 
                               "password": inputPassword.value as String
                               }
                           }
                       }).then ((res: any) => {
                           if (res != null) {
                                inputEmail.value = "";
                                inputPassword.value = "";
                                authService.setUserToken(res.data.login.jWT);
                                navigate();
                           }  
                       }).catch(e => {});
                    }
                 }}>
                    <div className="col-sm">
                        <div className="signin__header">IMPACT R&D</div>
                    </div>
                    <div className="col-sm signin__input" >
                        <div className={`signin__input__label ${isEmailActive? 'signin__input__label_is-active' : ''}`}>Email</div>
                        <input 
                            ref={node=> {inputEmail = node}}
                            className="signin__input__field" 
                            type="email" 
                            required={true} 
                            onFocus={() => this.setState({isEmailActive: true})} 
                            onBlur={() => this.onBlur(inputEmail.value as string, "isEmailActive")}
                        />
                    </div>
                    <div className="col-sm signin__input">
                        <div className={`signin__input__label ${isPasswordActive? 'signin__input__label_is-active' : ''}`}>Password</div>
                        <input 
                            className="signin__input__field" 
                            type="password" 
                            required={true} 
                            ref={node=> inputPassword = node}
                            onFocus={() => this.setState({isPasswordActive: true})}  
                            onBlur={() => this.onBlur(inputPassword.value as string, "isPasswordActive")} 
                        />
                    </div>
                    <div className="col-sm">
                        <div className="signin__input_button">
                            <ImpactButton 
                                color={"black"} 
                                text={"Login"}
                            />
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <div className="signin__signup">
                            <span className="signin__signup__text">not a user yet? </span>
                            <span 
                                className="signin__signup__text signin__signup_signup"
                                onClick={() => showContent("signup")}>
                                sign up
                            </span>
                        </div>
                    </div>
                 </form>
                )}
            </Mutation>
        );
    }
}

export default SignIn;