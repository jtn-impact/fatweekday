import * as React from 'react';
import { Mutation } from "react-apollo";
import { SIGN_UP } from '../../GraphQLStatements/GraphQLStatements';
import { AuthenticationService } from '../../Services/Authentication.service';
import { ValidationService } from '../../Services/Validation.service';
import ImpactButton from '../../Main/ImpactButton';
import { ILogin } from '../../Models/ILogin';

class SignUp extends React.Component<ILogin, any> {
    
    state = {
        isEmailActive: false,
        isPasswordActive: false,
        isPasswordMatchActive: false,
    }

    onBlur = (value: string, stateObject: string) => {
        if (value.length != 0 ) {
            this.setState({[stateObject]: true})
        }
        else {
            this.setState({[stateObject]: false})
        }
    }

    render() {

    const { isEmailActive, isPasswordActive, isPasswordMatchActive } = this.state
    const { navigate, showContent } = this.props;

    let authService = new AuthenticationService();
    let validService = new ValidationService()

    let inputEmail : any;
    let inputPassword : any;
    let inputPassswordMatch : any;

    return (
        <Mutation mutation={SIGN_UP}>
            {(signUp) => (
             <form className="signin__content" onSubmit={e => {
                e.preventDefault();
                if (validService.IsRegisterInputValid(inputEmail.value, inputPassword.value, inputPassswordMatch.value)) {
                    signUp({
                        variables: { 
                           user: {
                           "email": inputEmail.value as String, 
                           "password": inputPassword.value as String
                           }
                       }
                   }).then ((res: any) => {
                       if(res != null) {
                            inputEmail.value = "";
                            inputPassword.value = "";
                            inputPassswordMatch.value = "";
                            authService.setUserToken(res.data.register.jWT);
                            navigate();
                       }
                   }).catch(e => {
                   });
                }
             }}>
                <div className="col-sm">
                        <div className="signin__header">IMPACT R&D</div>
                    </div>
                    <div className="col-sm signin__input" >
                        <div className={`signin__input__label ${isEmailActive? 'signin__input__label_is-active' : ''}`}>Email</div>
                            <input 
                                ref={node=> {inputEmail = node}}
                                className="signin__input__field" 
                                required={true} 
                                type="email" 
                                onFocus={() => {this.setState({isEmailActive: true})}} 
                                onBlur={() => {this.onBlur(inputEmail.value as string, "isEmailActive")}} 
                            />
                    </div>
                    <div className="col-sm signin__input">
                        <div className={`signin__input__label ${isPasswordActive? 'signin__input__label_is-active' : ''}`} >Password</div>
                        <input 
                                ref={node=> {inputPassword = node}}
                                className="signin__input__field" 
                                required={true} 
                                type="password" 
                                onFocus={() => {this.setState({isPasswordActive: true})}} 
                                onBlur={() => {this.onBlur(inputPassword.value as string, "isPasswordActive")}} 
                            />
                    </div>
                    <div className="col-sm signin__input">
                        <div className={`signin__input__label ${isPasswordMatchActive? 'signin__input__label_is-active' : ''}`} >Confirm Password</div>
                            <input 
                                ref={node=> {inputPassswordMatch = node}}
                                className="signin__input__field" 
                                required={true} 
                                type="password" 
                                onFocus={() => {this.setState({isPasswordMatchActive: true})}} 
                                onBlur={() => {this.onBlur(inputPassswordMatch.value as string, "isPasswordMatchActive")}} 
                            />
                    </div>
                    <div className="col-sm">
                        <div className="signin__input_button">
                            <ImpactButton 
                                color={"black"} 
                                text={"Sign up"}
                            />
                        </div>
                    </div>
                    <div className="col-sm-8">
                        <div className="signin__signup">
                            <span className="signin__signup__text">Already a user?</span>
                            <span 
                                className="signin__signup__text signin__signup_signup"
                                onClick={() => {showContent("signin")}}>
                                sign in 
                            </span>
                        </div>
                    </div>
             </form>
            )}
        </Mutation>
        );
    }
}

export default SignUp;
