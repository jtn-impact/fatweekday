import * as React from 'react';
import SignUp from './SignUp';
import SignIn from './SignIn';
import { withRouter } from 'react-router-dom';

class Authentication extends React.Component<any, any>  {
    constructor(props : any) {
        super(props)
        this.state = {
            showLogin : true,
            showSignup : false,
        }       
    }

    authenticationSuccesfull = () => {
        this.props.history.push('/overview')
    } 
    
    showContent = (content : string) => {
        if (content == "signin")
        {
            this.setState({showLogin: true, showSignup: false});
        }
        if (content == "signup")
        {
            this.setState({showLogin: false, showSignup: true});
        }
    }

    render() {

        const { setAuth } = this.props;
        
        return (
            <div className="signin container">
                {this.state.showLogin ? (
                    <SignIn 
                        setAuth={setAuth} 
                        navigate={this.authenticationSuccesfull} 
                        showContent={this.showContent}
                    />
                ) : (
                    <SignUp 
                        setAuth={setAuth} 
                        navigate={this.authenticationSuccesfull} 
                        showContent={this.showContent}
                    />
                )}
            </div>
        )
    }
}
export default withRouter(Authentication);