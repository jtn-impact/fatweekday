import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import App from './app';


const client = new ApolloClient({ 
	uri: "https://rnd-api.azurewebsites.net/api/graphql",
	fetchOptions : {
		credentials : 'include'
	},
	request: async (operation) => {
			try {
				const token = await localStorage.getItem('Token');
				operation.setContext({
					headers: {
						authorization: token
					}
				});
			} catch (e) {
				console.log(e)
			}
  },
});

ReactDOM.render(
	<ApolloProvider client={client}>
		<App />
		<img id="impact-image" src="https://www.impact.dk/wp-content/uploads/2018/02/IMG_5600-1-1920x1080.jpg"></img>
	</ApolloProvider>,
	document.getElementById('root') as HTMLElement
);