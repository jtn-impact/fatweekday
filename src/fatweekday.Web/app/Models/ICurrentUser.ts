export interface ICurrentUser {
    isAdmin: number,
    userID: number,
    email: string,
}