import { ILikes } from './ILikes';

export interface IComment {
    author: string;
    text: string;
    creationDate: string;
    likeCount : number;
    iD : number;
    userID : number,
    likes : ILikes[]
}