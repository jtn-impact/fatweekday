import { ILikes } from './ILikes';

export interface IPitch {
    iD: number;
    author: string,
    title: string,
    effort: number,
    likeCount: number,
    description: string,
    creationDate: any,
    commentCount: number,
    isApproved: any,
    likes: ILikes[]
}