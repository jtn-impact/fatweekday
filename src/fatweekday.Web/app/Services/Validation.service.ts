export class ValidationService {

    private regexEmail = RegExp('[\w0-9\.-]*@impact\.dk');
    private regexPassword = RegExp('(?=.*[A-Z])'); //TODO: Find good Regex pattern for password checking.  
    private regexDescription = RegExp('^.{5,}$');
    private regexEffort = RegExp('^[1-9][0-9]*$');
    private regexTitle = this.regexDescription; //TODO: Consider what the Regex should be for title.
    private regexComment = this.regexDescription; //TODO: Consider what the Regex should be for Comment.
    
    IsRegisterInputValid = (email : string, password : string, passwordMatch : string) => {
        if(this.regexEmail.test(email) == false) 
        {
            console.log("Must be a valid IMPACT mail");
            return false;
        }
        if(password.length < 4)
        {
            console.log("The Password must consist of: Minimum four characters")
            return false;
        }
        else {
            if(password === passwordMatch)
            {
                return true;
            }
            else
            {  
                console.log("Passwords must match");
                return false;
            }
        }
    }

    isLoginValid = (email : string, password : string) => {
        if(this.regexEmail.test(email) == false) 
        {
            console.log("Must be a valid IMPACT mail");
            return false;
        }
        if(password.length < 4)
        {
            console.log("The Password must consist of: Minimum four characters")
            return false;
        }
        else
        {
            return true;
        }
    }

    IsPitchValid = (title: string, description: string, effort: string) => {
        if (this.regexTitle.test(title) == false)
        {
            console.log("Title must be more than 5 chars")
            return false
        }
        else if (this.regexDescription.test(description) == false) 
        {
            console.log("Description must be more than 5 chars")
            return false
        }    
        else if (this.regexEffort.test(effort) == false)
        {
            console.log("Effort must be a number bigger than zero")
            return false
        }
        else 
            return true
    }

    IsCommentValid = (text : string) => {
        if (this.regexComment.test(text) == true)
            return true
        else 
        {
            console.log("Comments must have a length of more than five chars");
            return false
        }
    }

};