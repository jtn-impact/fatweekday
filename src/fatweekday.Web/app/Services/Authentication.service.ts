import * as JWT from 'jwt-decode';

export class AuthenticationService {

    decodeToken = () : any => {
        var token : any = localStorage.getItem("Token")
        if(token !== null) {
            var decoded_token = JWT(token);
            if (decoded_token != null) 
            {
                return decoded_token;
            }
        }
    }

    isUserAuthenticated = () => {
        let token = localStorage.getItem("Token");
        if (token !== null)
        {
            var decoded_token = this.decodeToken()
            var current_time = new Date().getTime() / 1000;
            if (decoded_token.exp > current_time)
            {
                return true;
            }
            return false;
        }   
        else 
            return false;
    }

    setUserToken = (token : string) => {
        localStorage.setItem("Token", token);
    }
};