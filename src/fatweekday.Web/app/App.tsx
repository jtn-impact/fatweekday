import Authentication from './Authentication';
import Main from './Main'; 
import * as React from 'react';
import { Switch, Route, BrowserRouter as Router, Redirect} from 'react-router-dom';
import './styles/styles.scss';
import { AuthenticationService} from './Services/Authentication.service';

class App extends React.Component<any, any> {

    AuthService = new AuthenticationService();
    state = {
        userIsAuth : false,
    } 

    redirectTo = (isAuthenticated : any) => {
        if(isAuthenticated())
        {
            return "/overview"
        }
        else 
            return "/login"
    }
    
    public render() {

        const PrivateRoute = ({component, isAuthenticated, ...rest}: any) => {
            const routeComponent = (props: any) => (
                isAuthenticated()
                    ? React.createElement(component, props)
                    : <Redirect to={{pathname: '/login'}}/>
                );
            return <Route {...rest} render={routeComponent}/>;
        };

        return (
            <Router>
                <Switch>
                    <Route path='/login' component={Authentication}/>
                    <PrivateRoute
                    path='/overview'
                    isAuthenticated={this.AuthService.isUserAuthenticated}
                    component={Main}/>
                    <Redirect to={this.redirectTo(this.AuthService.isUserAuthenticated)}/>
                </Switch>
            </Router>
        )
    }
};

export default App;