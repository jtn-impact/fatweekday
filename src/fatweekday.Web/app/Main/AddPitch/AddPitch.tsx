import * as React from 'react';
import { Mutation } from 'react-apollo';
import { ADD_PITCH } from '../../GraphQLStatements/GraphQLStatements'
import ImpactButton from '../ImpactButton';
import { ValidationService } from '../../Services/Validation.service';
import { ICurrentUser } from '../../Models/ICurrentUser';

export interface IAddPitchProps {
    onCreate : any,
    currentUser: ICurrentUser,
}

class AddPitch extends React.Component<IAddPitchProps> {

    state = {
        isTitleActive: false,
        isDescriptionActive: false,
        isEffortActive: false,
    }
    
    onBlur = (value: string, stateObject: string) => {
        if (value.length != 0 ) {
            this.setState({[stateObject]: true})
        }
        else {
            this.setState({[stateObject]: false})
        }
    }

    render() {

    const { isTitleActive, isDescriptionActive, isEffortActive } = this.state;
    const { onCreate, currentUser} = this.props;

    let validService = new ValidationService();

    let inputTitle: any;
    let inputDescription: any;
    let inputEffort: any;
    
    let initials: string = currentUser.email.split(/@/)[0];
    
    
    return (
        <Mutation mutation={ADD_PITCH}>{(addPitch) => (
                <form
                    onSubmit={e => {
                        e.preventDefault();
                            if (validService.IsPitchValid(inputTitle.value, inputDescription.value, inputEffort.value)) {
                            addPitch({
                                variables: {
                                    pitch: {
                                        "userID": currentUser.userID,
                                        "title": inputTitle.value as String,
                                        "description": inputDescription.value as String, // TODO: Casting to String/Number because they're undefined from the input fields.
                                        "effort": inputEffort.value as Number,
                                        "author": initials
                                    }
                                }
                            }).then((res : any) => {
                                    inputTitle.value = "";
                                    inputDescription.value = "";
                                    inputEffort.value = "";
                                    onCreate();
                                    (document as any).location.href = (document as any).location.href
                            }).catch(e => {});
                        }
                        }}>   
                        <div className="col-sm-10 offset-sm-1 add-pitch__input">
                                <div className={`add-pitch__input__label ${isTitleActive? 'add-pitch__input__label_is-active' : ''}`}>Name your pitch</div>
                                <input 
                                    ref={node=> {inputTitle = node}} 
                                    className={`add-pitch__input__title ${isTitleActive? 'add-pitch__input__title_is-active' : ''}`} 
                                    minLength={5} 
                                    type="text" 
                                    required={true}
                                    onFocus={() => this.setState({isTitleActive: true})}
                                    onBlur={() => this.onBlur(inputTitle.value as string, "isTitleActive")}
                                />
                        </div>
                        <div className="col-sm-10 offset-sm-1 add-pitch__input">
                                <div className={`add-pitch__input__label ${isDescriptionActive? 'add-pitch__input__label_is-active' : ''}`}>Add description...</div>
                                <textarea 
                                    ref={node=> {inputDescription = node}} 
                                    className={`add-pitch__input__description ${isDescriptionActive? 'add-pitch__input__description_is-active' : ''}`} 
                                    minLength={5} 
                                    required={true}
                                    onFocus={() => this.setState({isDescriptionActive: true})}
                                    onBlur={() => this.onBlur(inputDescription.value as string, "isDescriptionActive")}
                                />
                        </div>
                        <div className="col-sm-10 offset-sm-1 add-pitch__input">
                            <div className={`add-pitch__input__label ${isEffortActive? 'add-pitch__input__label_is-active' : ''}`}>How many hours to dedicate to the pitch</div>
                            <input 
                                ref={node=> {inputEffort = node}} 
                                className="add-pitch__input__effort" 
                                min={1} type="number"
                                onFocus={() => this.setState({isEffortActive: true})}
                                onBlur={() => this.onBlur(inputEffort.value as string, "isEffortActive")}
                            />
                        </div>
                        <div className="col-sm-10 offset-sm-1">
                            <div className="add-pitch__button">
                                <ImpactButton 
                                    color={"black"} 
                                    text={"Submit Pitch"}
                                />
                            </div>
                        </div>
                </form>
        )}
        </Mutation>
    );
    }
}
export default AddPitch;