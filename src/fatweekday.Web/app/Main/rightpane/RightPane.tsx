import * as React from 'react';

interface IRightPaneProps {
    isShowing: boolean
    onExit : any
}

let counter = 0;
let isInitialized = false;

const RightPane: React.SFC<IRightPaneProps> = (props) => {
    
    const { onExit, children, isShowing } = props;
    
    if (counter !== 0) {
        isInitialized = true;
    }
    counter++;

    return (
        isShowing ?
            <div className="right-pane">
                <div className="col-sm-2 offset-sm-10">
                    <i className="fas fa-times" id="exit-pane" onClick={()=> {onExit()}}></i>
                </div>
                { children }
            </div>
                : isInitialized ?
            <div className="right-pane right-pane_slide-out"> 
                { children }
            </div>
            : null
    );
};

export default RightPane;