﻿import * as React from 'react';
import LikeButton from '../../LikeButton';
import ApprovePitchButton from './ApprovePitchButton';
import {ADD_LIKE_PITCH, REMOVE_LIKE_FROM_PITCH} from '../../../GraphQLStatements/GraphQLStatements';
import { IPitch } from '../../../Models/IPitch';
import { ICurrentUser } from '../../../Models/ICurrentUser';

interface IPitchItemProps {
	pitch : IPitch,
	onShowSpecificPitch: any,
	currentUser : ICurrentUser,
}

class PitchItem extends React.Component<IPitchItemProps, any> {
	
	isAdmin = (approved: number, isAdmin: number) => {
		if(approved === 0 && isAdmin === 1)
			return true
		else
			return 
	}

	isLiked = () => {
		if(this.props.pitch.likes) {
			const like = this.props.pitch.likes.find((like: any) => like.userID == this.props.currentUser.userID);
			if (like) {
				return true;
			}
			return false;
		}
		return false
	}

	AddOrRemoveQuery = () => {
		if (this.isLiked()) {
			return REMOVE_LIKE_FROM_PITCH
		}
		return ADD_LIKE_PITCH
	}

	render() {
		const { pitch, onShowSpecificPitch, currentUser } = this.props;

		return (
			<div className={pitch.isApproved === 1 ? ( "card pitch" ) : "card pitch pitch_prospect" } 
				onClick={() => onShowSpecificPitch(pitch.iD, pitch.isApproved)} >
				<div className="pitch__item">
					<div className="pitch__item__title">{pitch.title}</div>
					<div className="pitch__item__effort">{pitch.effort} HRS <i className="far fa-clock icon"></i></div>
				</div>
				<div className="pitch__metadata">Created by	<span className="pitch__metadata__author">{pitch.author}</span>, {pitch.creationDate}</div>
				<div className="pitch__description">{pitch.description}</div>
				<div id="divide-line-pitch" />
				<div className="pitch__item__social">
					<div className="pitch__item__social__comment"><i className="far fa-comment-alt like__icon"></i>{pitch.commentCount}</div>
					<div className="pitch__item__social__likes">
						<LikeButton 
							isLiked={this.isLiked()} 
							iD={pitch.iD} 
							userID={currentUser.userID} 
							query={this.AddOrRemoveQuery()} 
							likeType={"pitchID"} 
						/> {pitch.likeCount}
					</div>
				</div>
				{this.isAdmin(pitch.isApproved, currentUser.isAdmin) ? (
					<ApprovePitchButton 
						pitchID={pitch.iD} 
					/>
				) : null }
			</div>
		);
	}
	}

export default PitchItem;