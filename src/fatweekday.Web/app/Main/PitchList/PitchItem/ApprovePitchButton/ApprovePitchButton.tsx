import * as React from 'react';
import { Mutation } from 'react-apollo';
import { APPROVE_PITCH } from '../../../../GraphQLStatements/GraphQLStatements'

interface IApprovePitchProps {
    pitchID : number,
}

const ApprovePitchButton : React.SFC<IApprovePitchProps> = (props: any) => {

    const { pitchID } = props;

    return (
        <Mutation 
            mutation={APPROVE_PITCH}>
			    {(approvePitch) => (
                    <div className="pitch__approval__button"
                        onClick={e => {
                            e.preventDefault();
                            e.stopPropagation();
                            approvePitch({ variables : { "id": pitchID} 
                            }).then((res: any) => {
                                (document as any).location.href = (document as any).location.href
                            });
				    }}>APPROVE PITCH</div>
                )}
        </Mutation>  
    );
};

export default ApprovePitchButton;