import { Query } from 'react-apollo';
import PitchItem from './PitchItem';
import * as React from 'react';
import { ICurrentUser } from '../../Models/ICurrentUser';

interface IPitchList {
    onShowSpecificPitch : any,
    isApproved: number,
    query: any,
    currentUser: ICurrentUser
}

const PitchList: React.SFC<IPitchList> = (props: any) => { 

    const { query, isApproved, currentUser, onShowSpecificPitch } = props;

    return(
        <div className="pitch-list-container">
            <Query query={query} 
            pollInterval={30000}
            variables={{ approved: isApproved}}>
                {({ loading, error, data }) => {
                    if (loading) return <p>Loading...</p>;
                    if (error) return <p>Can't contact server</p>; 
                    return (
                        <div className="card-columns">
                            {data.pitchesByApprovedStatus.map((pitch: any, index: number) => {
                                if (data.pitchesByApprovedStatus != null) {
                                    return (
                                        <PitchItem
                                            key={index}
                                            pitch={pitch}
                                            currentUser={currentUser}
                                            onShowSpecificPitch={onShowSpecificPitch} 
                                        />
                                    ); 
                                }
                            })}
                        </div>
                        );
                    }}
            </Query>
        </div>
    );
    
}

export default PitchList;