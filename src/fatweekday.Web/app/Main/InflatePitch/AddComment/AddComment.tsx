import * as React from 'react';
import { ADD_COMMENT } from '../../../GraphQLStatements/GraphQLStatements';
import { Mutation } from 'react-apollo';
import ImpactButton from '../../ImpactButton';
import GetUserImage from '../GetUserImage';
import { ValidationService } from '../../../Services/Validation.service';
import { ICurrentUser } from '../../../Models/ICurrentUser';

interface IAddCommentProps {
    selectedPitch : {
        iD: number,
    },
    currentUser: ICurrentUser
}

const AddComment: React.SFC<IAddCommentProps> = (props: any) => { 

    const { selectedPitch, currentUser } = props;

    let inputText : any;
    let initials : string = currentUser.email.split(/@/)[0];
    let validService = new ValidationService();

    return (
            <Mutation 
                mutation={ADD_COMMENT}>
                    {(addComment) => (
                        <form 
                            onSubmit={e => {
                            e.preventDefault();
                            if (validService.IsCommentValid(inputText.value)) {
                                e.stopPropagation();
                                addComment({ 
                                    variables : { 
                                        comment : { 
                                            "text": inputText.value as String,
                                            "pitchID": selectedPitch.iD,
                                            "userID": currentUser.userID,
                                            "author": initials
                                        }
                                    } 
                                }).then((res : any) => {
                                    inputText.value = "";
                                    (document as any).location.href = (document as any).location.href
                                }).catch(e => {
                                });
                            }
                            inputText.value = "";
                        }}>
                        <div className="addComment">
                            <div className="row addComment__item">
                                <div className="col-sm-1 offset-sm-1">
                                    <GetUserImage 
                                        commentUserID={currentUser.userID}
                                    />
                                </div>
                                <textarea 
                                    ref={node => {inputText = node}}
                                    className="col-sm-8 addComment__text" 
                                    placeholder="Pssttch... throw a comment" 
                                    minLength={5} required={true} 
                                />
                                <div className="col-sm-8 offset-sm-2">
                                    <div className="impact-button_addcomment">
                                        <ImpactButton 
                                            color={"white"} 
                                            text={"Add Comment"}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                )}
            </Mutation>
    );
}

export default AddComment;