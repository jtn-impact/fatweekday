import * as React from 'react';
import Comment from './Comment';
import PitchDetail from './PitchDetail';
import AddComment from './AddComment';
import { GET_PITCH_BY_ID } from '../../GraphQLStatements/GraphQLStatements';
import { Query } from 'react-apollo';

interface IInflatePitchProps {
    selectedPitch : {
        iD: number,
    }
    currentUser: any,
}

const InflatePitch: React.SFC<IInflatePitchProps> = (props : any) => { 

    const { selectedPitch, currentUser } = props;

    return(
        <React.Fragment>
            <Query 
            query={GET_PITCH_BY_ID}
            pollInterval={30000}
            variables={{ id: selectedPitch.iD}}>
                {({ loading, error, data}) => {
                    if (loading) return <p>Loading...</p>;
                    if (error) return <p>Can't contact server</p>; 
                    return (
                        <React.Fragment>
                            <PitchDetail pitch={data.pitchByID} selectedPitch={selectedPitch}/>
                            <div className="comments">
                                {data.pitchByID.comments.map((comment : any, index : number) => {
                                    return (
                                        <Comment 
                                        key={index}
                                        comment={comment}
                                        currentUser={currentUser}
                                    />
                                    )
                                })}
                            </div>
                        </React.Fragment>
                    );
                }}
            </Query>
            <AddComment selectedPitch={selectedPitch} currentUser={currentUser} />
        </React.Fragment>
    ) 
        
};
    

export default InflatePitch;