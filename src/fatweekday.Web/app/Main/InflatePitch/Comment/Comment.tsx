import * as React from 'react';
import LikeButton from '../../LikeButton';
import GetUserImage from '../GetUserImage';
import {ADD_LIKE_COMMENT, REMOVE_LIKE_FROM_COMMENT} from '../../../GraphQLStatements/GraphQLStatements'
import { ICurrentUser } from '../../../Models/ICurrentUser';
import { IComment } from '../../../Models/IComment';

interface ICommentProps {
    comment: IComment,
    currentUser: ICurrentUser,
}

const Comment: React.SFC<ICommentProps> = (props : any ) => {

    const { comment, currentUser } = props;

    const isLiked = () => {
        const like = comment.likes.find((like: any) => like.userID == currentUser.userID);
        if (like) {
            return true;
        }
        return false;
    }

    const AddOrRemoveQuery = () => {
        if (isLiked()) {
            return REMOVE_LIKE_FROM_COMMENT
        }
        return ADD_LIKE_COMMENT
    }

    return ( 
        <div className="comment">
            <div className="row">
                <div className="col-sm-1 offset-sm-1">
                    <GetUserImage 
                        commentUserID={comment.userID} 
                    />
                </div>
                <div className="col-sm-8 comment__container">
                    {comment.text}
                </div>
                <div className="col-9-sm offset-sm-2">
                    <span className="comment__meta-data">Comment by: <span className="comment__meta-data_author">{comment.author}</span> {comment.creationDate}</span>
                    <LikeButton 
                        isLiked={isLiked()} 
                        iD={comment.iD} 
                        userID={currentUser.userID} 
                        query={AddOrRemoveQuery()} 
                        likeType={"commentID"}
                    /> {comment.likeCount} 
                </div>
            </div>
        </div>
    );
};

export default Comment;