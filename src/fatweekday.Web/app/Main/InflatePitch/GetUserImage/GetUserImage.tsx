import * as React from 'react';
import { Query } from 'react-apollo';
import { GET_USERIMAGE_BY_ID } from '../../../GraphQLStatements/GraphQLStatements';

interface IGetUserImageProps {
	commentUserID : number,
}

const GetUserImage: React.SFC<IGetUserImageProps> = (props : any) => {

    const { commentUserID } = props;
    
    return (
        <Query 
            query={GET_USERIMAGE_BY_ID} 
            variables={{id : commentUserID}}>
                {({loading, error, data}) => {
                    if (loading) return <p>Loading...</p>;
                    if (error) return <p>Can't contact server</p>;
                    return (
                    <React.Fragment>
                        {data.userByID.imageURL != null ? (
                            <img 
                                className="comment__image" 
                                src={data.userByID.imageURL}
                            />
                        ) : (
                            <img 
                                className="comment__image" 
                                src="https://www.impact.dk/wp-content/themes/impact/assets/img/employee-fallback.jpg"
                            />
                        )}
                    </React.Fragment>
                    )
                }}
        </Query>
    )
}
export default GetUserImage;