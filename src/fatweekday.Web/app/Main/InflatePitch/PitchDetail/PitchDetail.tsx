import * as React from 'react';
import { IPitch } from '../../../Models/IPitch';

interface IPitchDetailProps {
	selectedPitch: {
		iD: number,
		isApproved: number,
	}
	pitch: IPitch,
}

const PitchDetail : React.SFC<IPitchDetailProps> = (props: any) => {

	const { selectedPitch, pitch } = props;
    
	return (
		<div className={selectedPitch.isApproved === 1 ? ("") : ("pitch-detail_prospect")}>
			<div className="row">
				<div className="col-sm-6 offset-sm-1 pitch-detail__title">{pitch.title}</div>
				<div className="col-sm-3 pitch-detail__effort">{pitch.effort} HRS <i className="far fa-clock icon"></i></div>
				<div className="col-sm-11 offset-sm-1 pitch-detail__metadata">Pitch Created by <span className="pitch-detail__metadata_author">{pitch.author}</span>, {pitch.creationDate}</div>
				<div className="col-sm-10 offset-sm-1 pitch-detail__description">{pitch.description}</div>
			</div>
		</div>
	)

}

export default PitchDetail;