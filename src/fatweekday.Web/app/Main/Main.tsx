﻿import * as React from 'react';
import RightPane from "./rightpane";
import AddPitch from './AddPitch';;
import { AuthenticationService } from '../Services/Authentication.service';
import { withRouter } from 'react-router-dom';
import ImpactButton from './ImpactButton';
import PitchList from './PitchList';
import {GET_PITCHES_BY_STATUS} from '../GraphQLStatements/GraphQLStatements';
import InflatePitch from './InflatePitch';

class Main extends React.Component<any, any> {

    AuthSerivce = new AuthenticationService();

    constructor(props: any){
        super(props);
        this.state = {
            currentUser : {
                userID: null,
                isAdmin: 0,
                email: null,
                userImage: null,
            },
            selectedPitch: {
                iD: null,
                isApproved: null,
            },
            isRightPaneShowing: false,
            hasRightPaneBeenShown: false,
            isCreatePitchShowing: false,
            isSpecificPitchShowing: false,
            activeElement: "all",
            isApproved: 2,
            pitchListQuery: GET_PITCHES_BY_STATUS,
        };
    }

    private onCreatePitch = () => {
        const paneIsShowing = !this.state.isRightPaneShowing;
        this.resetSideBar()
        this.setState({isRightPaneShowing: paneIsShowing, isCreatePitchShowing: true, hasRightPaneBeenShown: true});
    };

    private onExit = () => {
        const paneIsShowing = !this.state.isRightPaneShowing;
        this.setState({isRightPaneShowing: paneIsShowing, isCreatePitchShowing: false, isSpecificPitchShowing: false});
    };

    private OnExitFromCreate = () => {
        const paneIsShowing = !this.state.isRightPaneShowing;
        this.setState({isRightPaneShowing: paneIsShowing, isCreatePitchShowing: false, isSpecificPitchShowing: false, activeElement: "prospect", approved: 0});
    }

    private onShowSpecificPitch = (iD : number, isApproved : number) => {
        this.setState({selectedPitch: {
            iD: iD,
            isApproved: isApproved,
        }});
        this.resetSideBar()
        this.setState({isRightPaneShowing: true, isSpecificPitchShowing: true, hasRightPaneBeenShown: true});
    }

    private resetSideBar = () => {
        this.setState({isCreatePitchShowing: false, isSpecificPitchShowing: false});
    };

    componentDidMount() {
        this.getUserFromToken();
    }

    private getUserFromToken = () => {
        let decoded_token = this.AuthSerivce.decodeToken();
        if (decoded_token != null)
        {
            this.setState({
                currentUser : {
                    userID : decoded_token.userId,
                    isAdmin : decoded_token.isAdmin,
                    email : decoded_token.email,
                }
            });
        }
    }

    private logOut = () => {
        localStorage.removeItem("Token");
        this.props.history.push('/login')
    }

    private sortPitch = (divName : string, approved : number) => {
        this.setState({activeElement: divName, isApproved: approved})
    }; 

    public render() {
        const { activeElement, isApproved, pitchListQuery, currentUser, isRightPaneShowing, selectedPitch, isSpecificPitchShowing, isCreatePitchShowing } = this.state;

        return (
            <div className="app container">
                <div className="row header">
                    <div className="col-sm-7 ">
                        <div className="header__text">IMPACT <span id="header-inline-text">R&D</span> PITCHES</div>			
                    </div>
                    <div className="col-sm-2 offset-sm-3 logout" onClick={() => {this.logOut()}}>
                        <div className="logout__item">
                            <span className="logout__text">Sign out</span>
                            <i className="fas fa-arrow-right logout__icon"></i>
                            <div className="logout__underline"></div>
                        </div>
                    </div>
                </div>
                
                <p className="view">FILTERS</p>
                <div className="row">
                    <div className="col-sm-5">
                        <div onClick={()=> this.sortPitch('all', 2)} className={activeElement === 'all' ? "view__options view_marked" : "view__options"}>
                            <div>All</div>
                            <div className="view__underline"></div>
                        </div>
                        <div onClick={()=> this.sortPitch('approved', 1)} className={activeElement === 'approved' ? "view__options view_marked" : "view__options"}>
                            <div>Approved</div>
                            <div className="view__underline"></div>
                        </div>
                        <div onClick={()=> this.sortPitch('prospect', 0)} className={activeElement === 'prospect' ? "view__options view_marked" : "view__options"}>
                            <div>Prospects</div>
                            <div className="view__underline"></div>
                        </div>
                    </div>

                    <div className="col-sm-3 offset-sm-4" onClick={() => {this.onCreatePitch()}}>
                        <div className="float-right">
                            <ImpactButton 
                                color={"white"} 
                                text={"Add Pitch"}
                            />
                        </div>
                    </div>
                    <PitchList 
                        isApproved={isApproved} 
                        onShowSpecificPitch={this.onShowSpecificPitch} 
                        query={pitchListQuery} 
                        currentUser={currentUser}
                    />
                </div>
                
                {this.state.hasRightPaneBeenShown ? (
                    <RightPane 
                        onExit={this.onExit} 
                        isShowing={isRightPaneShowing}
                    >
                    {isCreatePitchShowing ? (
                        <AddPitch 
                            onCreate={this.OnExitFromCreate} 
                            currentUser={currentUser} 
                        /> 
                    ) : null}
                    {isSpecificPitchShowing ? (
                        <InflatePitch 
                            selectedPitch={selectedPitch} 
                            currentUser={currentUser} 
                        />
                    ) : null}
                </RightPane>
                ) : null }
            </div>
        );
    }
}

export default withRouter(Main);