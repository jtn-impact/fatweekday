import * as React from 'react';

interface IImpactButtonProps {
    text: string,
    color: string,
}

const ImpactButton: React.SFC<IImpactButtonProps> = (props) => {
    
    const {
        color,
        text,
    } = props;
    
    return (
        <button type="submit" className="impact-button" id={color}>
            <div className="impact-button__bar"></div>
            <span className="impact-button__text">{text}</span>
            <i className="fas fa-arrow-right impact-button__icon"></i>
        </button>
    )
};

export default ImpactButton;