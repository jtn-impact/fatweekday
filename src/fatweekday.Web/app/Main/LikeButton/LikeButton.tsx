import * as React from 'react';
import { Mutation} from 'react-apollo';

interface ILikeButtonProps {
    userID : number;
    likeType : any;
    iD: any;
    query: any;
    isLiked?: boolean;
}

const LikeButton: React.SFC<ILikeButtonProps> = (props) => {
    
    const { userID, likeType, iD, query, isLiked } = props;

    return (
        <Mutation 
        mutation={query}>
			{(addLike) => {
                const likeClass = ["far fa-thumbs-up like__icon"];
                if (isLiked) {
                    likeClass.push("marked");
                }
                return (
                <i className={likeClass.join(" ")}
                    onClick={e => {
					e.preventDefault();
                    e.stopPropagation();
                    addLike({ variables : { like : 
                        { userID: userID,
                          [likeType]: iD}
                        } 
                    }).then ((res : any) => {
                        (document as any).location.href = (document as any).location.href
                    }).catch(e => {
                    });
				}}>
                </i>
            )}}
        </Mutation>
    );
};

export default LikeButton;