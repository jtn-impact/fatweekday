import { gql } from 'apollo-boost';


export const GET_USERIMAGE_BY_ID : any = gql`
    query GetUserByID($id: Int!) {
        userByID(ID: $id) {
            imageURL
        }
    }
`;


export const GET_PITCH_BY_ID : any = gql`
	query GetPitchByID($id: Int!) {
        pitchByID(ID: $id) {
            iD,
            title,
            effort,
            likeCount,
            commentCount,
            description,
            creationDate,
            isApproved,
            author,
            comments {
                author
                text
                creationDate
                likeCount
                iD
                userID
                likes {
                    userID
                }
            } 
        }
      }
`;

export const GET_PITCHES_BY_STATUS : any = gql`
    query GetPitchesByStatus($approved: Int!) {
        pitchesByApprovedStatus(Approved: $approved) {
            iD
			title
			effort
            description
            creationDate
            isApproved
            author
            likes {
                userID
            }
            likeCount
            commentCount
        }
    }
`;

export const GET_COMMENTS_BY_PITCHID : any = gql`
    query GetCommentsByPitchId($id: Int!) {
        commentsByPitchId(ID: $id) {
        id,
        text,
        likeCount,
        userId,
        creationDate,
        author
        }
    }
`;

export const ADD_PITCH : any = gql`
    mutation AddPitch($pitch: PitchInput!) {
        addPitch(Pitch: $pitch) {
            iD
        }
}
`;

export const APPROVE_PITCH : any = gql`
    mutation ApprovePitch($id: Int!) {
        approvePitch(ID: $id) {
            iD
        }
}
`;

export const ADD_LIKE_PITCH : any = gql`
    mutation AddLikePitch($like: PitchLikeInput!) {
        addLikePitch(PitchLike: $like) {
            iD
        }
    }
`;

export const REMOVE_LIKE_FROM_PITCH : any = gql`
    mutation RemoveLikeFromPitch($like: PitchLikeInput!)
    {
        removeLikeFromPitch(PitchLike: $like)
        {
            creationDate
            pitchID
            userID
        }
}
`;

export const ADD_LIKE_COMMENT : any = gql`
    mutation AddLikeComment($like: CommentLikeInput!) {
        addLikeComment(CommentLike: $like) {
            iD
        }
    }
`;

export const REMOVE_LIKE_FROM_COMMENT : any = gql`
    mutation RemoveLikeFromComment($like: CommentLikeInput!)
    {
    removeLikeFromComment(CommentLike: $like)
        {
            iD
            creationDate
            userID
            commentID
        }
    }
`;

export const ADD_COMMENT : any = gql `
    mutation AddComment($comment:CommentInput!) {
        addComment(Comment: $comment) {
            iD
    }
}
`;

export const SIGN_UP : any = gql`
    mutation Register($user: UserInput!) 
    {
    register(User: $user)
        {
            jWT
        }
    }
`;

export const LOGIN : any = gql `
    mutation Login($user: UserInput!)
    {
        login(User: $user)
        {
            jWT
        }
    }
`;



