﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.Web.Infrastructure
{
    public class AppSettings
    {
		public string GraphQLEndpoint { get; private set; }

		public AppSettings (IConfiguration configuration)
		{
			GraphQLEndpoint = configuration.GetValue<string>("GraphQL:Endpoint");
		}
    }
}
