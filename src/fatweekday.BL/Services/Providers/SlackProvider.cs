using fatweekday.Domain.Models;
using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using System.Collections.Generic;
using static fatweekday.BL.Services.Providers.SlackMessageModel;

namespace fatweekday.BL.Services.Providers
{
    public class SlackProvider 
    {
      private readonly string SlackMessageURL = "https://hooks.slack.com/services/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";
      public void newPitch(Pitch pitch)
      {

            var SlackHeaderMessage = "New pitch: " + pitch.Title;
            var SlackSubMessage = " Est. time of pitch " + pitch.Effort + " hrs";
            var Attachments = new Collection<SubText> 
            {
               new SubText(SlackSubMessage, pitch.Author)
            };

            SlackMessageModel slackMessageModel = new SlackMessageModel(SlackHeaderMessage, Attachments);
            string data = JsonConvert.SerializeObject(slackMessageModel, Formatting.Indented);
            
            using (var web = new WebClient()) 
            {
                var response = web.UploadString(SlackMessageURL, data);
            } 
      }
        public void pitchApproved(Pitch pitch)
        {
            var SlackHeaderMessage = "Pitch: " + pitch.Title + ", Submitted by: " +pitch.Author.ToUpper()+ ", Was approved by HBM"; //TODO: GET THE USER THAT APPROVED FROM TOKEN
            var SlackSubMessage = " Est. time of pitch " + pitch.Effort + " hrs";
            var Attachments = new Collection<SubText> 
            {
               new SubText(SlackSubMessage, pitch.Author)
            };

            SlackMessageModel slackMessageModel = new SlackMessageModel(SlackHeaderMessage, Attachments);
            string data = JsonConvert.SerializeObject(slackMessageModel, Formatting.Indented);
            
            using (var web = new WebClient()) 
            {
                var response = web.UploadString(SlackMessageURL, data);
            } 
        }
    }

    

    //SEE HOW TO STRUCTURE A SLACKMESSAGE LINK: https://api.slack.com/docs/messages/builder?msg=%7B%22attachments%22%3A%5B%7B%22fallback%22%3A%22Required%20plain-text%20summary%20of%20the%20attachment.%22%2C%22color%22%3A%22%2336a64f%22%2C%22pretext%22%3A%22Optional%20text%20that%20appears%20above%20the%20attachment%20block%22%2C%22author_name%22%3A%22Bobby%20Tables%22%2C%22author_link%22%3A%22http%3A%2F%2Fflickr.com%2Fbobby%2F%22%2C%22author_icon%22%3A%22http%3A%2F%2Fflickr.com%2Ficons%2Fbobby.jpg%22%2C%22title%22%3A%22Slack%20API%20Documentation%22%2C%22title_link%22%3A%22https%3A%2F%2Fapi.slack.com%2F%22%2C%22text%22%3A%22Optional%20text%20that%20appears%20within%20the%20attachment%22%2C%22fields%22%3A%5B%7B%22title%22%3A%22Priority%22%2C%22value%22%3A%22High%22%2C%22short%22%3Afalse%7D%5D%2C%22image_url%22%3A%22http%3A%2F%2Fmy-website.com%2Fpath%2Fto%2Fimage.jpg%22%2C%22thumb_url%22%3A%22http%3A%2F%2Fexample.com%2Fpath%2Fto%2Fthumb.png%22%2C%22footer%22%3A%22Slack%20API%22%2C%22footer_icon%22%3A%22https%3A%2F%2Fplatform.slack-edge.com%2Fimg%2Fdefault_application_icon.png%22%2C%22ts%22%3A123456789%7D%5D%7D
    public class SlackMessageModel
    {
        public string text { get; set; }

        public ICollection<SubText> attachments { get; set;}

        public SlackMessageModel(string text, Collection<SubText> attachments) 
        {
            this.text = text;
            this.attachments = attachments;

        }
        public class SubText 
        {
            public string text {get; set;}

            public string footer { get; set; }

            public string title { get; set; }

            public string title_link { get; set; }

            public int ts { get; set; }

            public string author_name { get; set; }

            public SubText(string text, string author)
            {
                this.text = text;
                this.footer = "IMPACTS Research & Development APP";
                this.author_name = author; //TODO: Change into Username when it's implemented
                this.title = "IMPACTS Research & Development";
                this.title_link = "http://localhost:63427/";
            }
        }
    }
}
