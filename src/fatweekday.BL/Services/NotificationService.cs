using fatweekday.Domain.Models;
using fatweekday.BL.Services.Providers;

namespace fatweekday.BL.Services
{
    public class NotificationService
    {
        public void newPitchNotification(Pitch pitch) {
            var SlackProvider = new SlackProvider();
            SlackProvider.newPitch(pitch);
        }
        public void pitchApprovedNotification(Pitch pitch) {
            var SlackProvider = new SlackProvider();
            SlackProvider.pitchApproved(pitch);
        }
        
    }
}


