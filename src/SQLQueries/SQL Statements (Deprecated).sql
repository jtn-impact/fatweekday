-- GET ALL PITCHES, AND ADD COLUMN OF LIKECOUNT AND COMMENTCOUNT TO EACH PITCH
SELECT * , 
(SELECT COUNT(*) FROM likes WHERE pitchId=P.id) AS likeCount , 
(SELECT COUNT(*) FROM comments WHERE pitchId=P.id) AS commentCount 
FROM pitches AS P

-- GET PITCH BY ID AND GET ALL COMMENTS ON THE PITCH
SELECT * FROM pitches as P
LEFT JOIN comments AS C ON P.id=C.pitchId WHERE p.id = 2

-- GET USER BASED ON EMAIL
SELECT id, initials, email, admin FROM users WHERE email='hbm@impact.dk' 