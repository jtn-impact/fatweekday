--PITCHES
INSERT INTO pitches (userId, title, description, effort, approved) 
VALUES (1, 'Learn React', 'Learn more about React. So we can make an implementation of it. To show the customers as a case!', 20, 1)

INSERT INTO pitches (userId, title, description, effort, approved)
VALUES (1, 'Learn Angular', 'Learn more about Angular. So we can make an implementation of it. To show the customers as a case!', 30, 0)

INSERT INTO pitches (userId, title, description, effort, approved)
VALUES (3, 'Learn MSSQL', 'Learn more about MSSQL. So we can make an implementation of it. To show the customers as a case!', 40, 0)

INSERT INTO pitches (userId, title, description, effort, approved)
VALUES (2, 'Learn GraphQL', 'Learn more about GraphQL. So we can make an implementation of it. To show the customers as a case!', 25, 1)

INSERT INTO pitches (userId, title, description, effort, approved)
VALUES (3, 'Learn Docker', 'Learn more about Docker. So we can make an implementation of it. To show the customers as a case!', 10, 1)

INSERT INTO pitches (userId, title, description, effort, approved)
VALUES (1, 'Learn CSS Grid System', 'Learn more about CSS Grid system. So we can make an implementation of it. To show the customers as a case!', 100, 1)

INSERT INTO pitches (userId, title, description, effort, approved)
VALUES (2, 'Learn Bootstrap', 'Learn more about Bootstrap. So we can make an implementation of it. To show the customers as a case!', 15, 1)

INSERT INTO pitches (userId, title, description, effort, approved)
VALUES (3, 'Learn .NET Core', 'Learn more about .NET Core. So we can make an implementation of it. To show the customers as a case!', 30, 1) 

INSERT INTO pitches (userId, title, description, effort, approved, author)
VALUES  (1, 'Lorem From SQL Database Azure', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 2, 0, 'hbm')

INSERT INTO pitches (userId, title, description, effort, approved, author)
VALUES  (2, 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 2, 0, 'hbm')


--COMMENTS
INSERT INTO comments (userId, pitchId, text)
VALUES (1, 2, 'Angular seems a bit old. Do you not think so?')

INSERT INTO comments (userId, pitchId, text)
VALUES (3, 2, 'No not at all!?')

INSERT INTO comments (userId, pitchId, text)
VALUES (1, 4, 'Angular seems a bit old. Do you not think so?')

INSERT INTO comments (userId, pitchId, text)
VALUES (2, 2, 'VueJS is old')

INSERT INTO comments (userId, pitchId, text)
VALUES (1, 2, 'I heard React is the shit')

INSERT INTO comments (userId, pitchId, text)
VALUES (2, 2, 'Angular is pretty new')

INSERT INTO comments (userId, pitchId, text)
VALUES (1, 3, 'MSSQL is old and not gold')

INSERT INTO comments (userId, pitchId, text)
VALUES (2, 3, 'Could not agree more!')

--LIKES
INSERT INTO likes (userId, pitchId)
VALUES (1,2)

INSERT INTO likes (userId, pitchId)
VALUES (1,3)

INSERT INTO likes (userId, pitchId)
VALUES (1,4)

INSERT INTO likes (userId, pitchId)
VALUES (1,6)

INSERT INTO likes (userId, pitchId)
VALUES (2,1)

INSERT INTO likes (userId, pitchId)
VALUES (2,3)

INSERT INTO likes (userId, pitchId)
VALUES (2,5)

INSERT INTO likes (userId, pitchId)
VALUES (3,2)

INSERT INTO likes (userId, pitchId)
VALUES (3,1)

INSERT INTO likes (userId, pitchId)
VALUES (3,5)

INSERT INTO likes (userId, pitchId)
VALUES (3,4)


